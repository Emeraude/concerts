#!/usr/bin/env node

const fs = require('fs').promises
const util = require('util')
const glob = util.promisify(require('glob'))
const jsmediatags = require('jsmediatags')
const parseArgs = require('minimist')
const asyncPool = require('tiny-async-pool')
const { searchArtists, getConcertsByArtist } = require('infoconcert-api')

async function readMetadataAsync(file) {
  return new Promise((resolve, reject) => {
    jsmediatags.read(file, {
      onSuccess: resolve,
      onError: reject
    })
  })
}

function verbose(string) {
  if (!argv.quiet) {
    process.stderr.write(string)
  }
}

async function asyncPoolWrapper(array, string, cb) {
  let count = 0
  verbose("\r" + string + ": " + count + "/" + array.length)
  const out = await asyncPool(100, array, async (a) => {
    const res = await cb(a)
    count += 1
    verbose("\r" + string + ": " + count + "/" + array.length)
    return res
  })
  verbose("\n")
  return out
}

async function getArtists(files = []) {
  const artists = {}
  await asyncPoolWrapper(files, "Analyzing music library", async (f) => {
    try {
      const { tags } = await readMetadataAsync(f) || {}
      if (tags && tags.artist) {
        artists[tags.artist] = true
      }
    } catch(e) {}
  })
  return Object.keys(artists).sort()
}

async function getAllConcerts(artists = []) {
  const concerts = await asyncPoolWrapper(artists, "Retrieving concerts", async (a) => {
    const [ artist ] = await searchArtists(a)
    return artist ? await getConcertsByArtist(artist) : []
  })
  return concerts.flat()
}

function help() {
  const argv0 = "concerts.js"
  console.log(`Usage: ${argv0} [options] [dir]...
        -f, --format    The output format, either "text", "json" or "raw". Default to "json"
        -h, --help      Display this message and exit
        -o, --output    The output file. Default is stdout
        -q, --quiet     Does not print anything`)
  process.exit(0)
}

function formatOutput(concerts) {
  if (argv.format === "text") {
    return concerts
      .map(concert => {
        return concert.date
          + " " + concert.time
          + " " + concert.artists.map(a => a.name).join(", ")
          + " " + concert.location.room.name + " (" + concert.location.city.department + ")"
          + " " + concert.status
          + (concert.price
             ? " " + concert.price.min.toLocaleString("fr-FR", {"style": "currency", currency: concert.price.currency})
             + "-" + concert.price.max.toLocaleString("fr-FR", {"style": "currency", currency: concert.price.currency})
             : "")
          + " " + (concert.reservation_url || " ")
      }).join("\n")
  } else {
    return JSON.stringify(concerts)
  }
}

(async function() {
  global.argv = parseArgs(process.argv.slice(2), { alias: {"h": "help",
                                                           "q": "quiet",
                                                           "f": "format",
                                                           "o": "output",
                                                           "c": "cached"}})
  if (argv.help)
    help()
  const files = []
  for (const [i, dir] of Object.entries(argv._.length > 0 ? argv._ : ["."])) {
    verbose("\rScanning directories: " + (parseInt(i) + 1) + "/" + argv._.length)
    files.push(await glob(dir + "/**", { nodir: true, nosort: true }))
  }
  verbose("\n")
  const artists = await getArtists([...new Set(files.flat())])
  const concerts = await getAllConcerts(artists)
  const output = formatOutput(concerts)
  if (argv.output && argv.output !== "-") {
    await fs.writeFile(argv.output, output)
  } else {
    console.log(output)
  }
})()
